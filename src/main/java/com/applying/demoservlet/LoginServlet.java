package com.applying.demoservlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
@WebServlet(urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {
 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	
    	String username = request.getParameter("username");
    	String password = request.getParameter("password");
    	
    	if("admin".equals(username)&& "password".equals(password)) {
    		request.getSession().setAttribute("AUTHENTICATED",username+password);
        	String nextJSP = "/upload.jsp";
        	RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
        	dispatcher.forward(request,response);
    	}else {
        	request.setAttribute("msg","usuario y/o contraseña incorrecta");
            response.sendRedirect(request.getContextPath() + "/login.jsp");
    	}

    }
 
    @Override
    public String getServletInfo() {
        return "Short description";
    }
    
}