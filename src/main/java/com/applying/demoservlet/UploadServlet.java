package com.applying.demoservlet;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
 
@WebServlet(asyncSupported = false, name = "UploadServlet", urlPatterns = {"/uploadServlet"},
initParams = {@WebInitParam(name="webInitParam1", value="Hello"), @WebInitParam(name="webInitParam2", value="World")})
@MultipartConfig(
maxFileSize = 10485760L, // 10 MB
maxRequestSize = 20971520L // 20 MB
)
public class UploadServlet extends HttpServlet {
 
	private static final String UPLOAD_DIR = "uploads";
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	String nextJSP = "/upload.jsp";
    	RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
    	dispatcher.forward(request,response);
    }
 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");

		Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
		    System.out.println("Header  " + headerNames.nextElement());
		}
		
		// gets absolute path of the web application
		String applicationPath = "C:\\archivos";
		//String applicationPath = request.getServletContext().getRealPath("");

		// constructs path of the directory to save uploaded file
		String uploadFilePath = applicationPath + File.separator + UPLOAD_DIR;

		// creates upload folder if it does not exists
		File uploadFolder = new File(uploadFilePath);
		if (!uploadFolder.exists()) {
			uploadFolder.mkdirs();
		}
	    OutputStream out = null;
	    InputStream filecontent = null;

		PrintWriter writer = response.getWriter();

		// write all files in upload folder
		for (Part part : request.getParts()) {
			if (part != null && part.getSize() > 0) {
				String fileName = part.getSubmittedFileName();
				System.out.println("fileName:"+fileName);
				String contentType = part.getContentType();
				
				// allows only JPEG files to be uploaded
//				if (!contentType.equalsIgnoreCase("image/jpeg")) {
//					continue;
//				}}
				/**/
				out = new FileOutputStream(new File(uploadFilePath + File.separator + fileName));
			    filecontent = part.getInputStream();
		        int read = 0;
		        final byte[] bytes = new byte[1024];

		        while ((read = filecontent.read(bytes)) != -1) {
		            out.write(bytes, 0, read);
		        }
	            out.close();
	            filecontent.close();

				/**/
				
				//part.write(uploadFilePath + File.separator + fileName);

				writer.append("File successfully uploaded to " 
						+ uploadFolder.getAbsolutePath() 
						+ File.separator
						+ fileName
						+ "<br>\r\n");
			}
		}
    }
 
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}